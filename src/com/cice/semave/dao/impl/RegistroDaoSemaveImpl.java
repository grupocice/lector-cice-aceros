
package com.cice.semave.dao.impl;

import com.cice.semave.dao.RegistroDaoSemave;
import com.cice.semave.db.Conexion;
import com.cice.semave.entity.Registro;
import com.cice.semave.proceso.Proceso;
import com.cice.semave.tools.Bitacora;
import com.cice.semave.tools.Propiedades;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RegistroDaoSemaveImpl extends Conexion implements RegistroDaoSemave {
    
    
    private Logger log = LogManager.getLogger(RegistroDaoSemaveImpl.class);
    
    public RegistroDaoSemaveImpl() {
        Propiedades propiedades = new Propiedades();
        propiedades.leerPropiedades("/com/cice/semave/gui/resources/mysqldatabase.properties");
        this.setDriver(propiedades.getDriver());
        this.setUrl(propiedades.getUrl());
        this.setUsuario(propiedades.getUsuario());
        this.setPassword(propiedades.getPassword());
    }
    
    public ArrayList<Registro> consultarRegistros() {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Registro> registros = new ArrayList<Registro>();
        
        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("ID, ");
                sentencia.append("CARD_NO, ");
                sentencia.append("CAST(CREATE_TIME AS CHAR) CREATE_TIME, ");
                sentencia.append("STATE_NAME , ");
                sentencia.append("device_name  ");
                sentencia.append("FROM ACC_MONITOR_LOG ");
                sentencia.append("WHERE CREATE_TIME >= CURDATE() ");
                sentencia.append("AND DELETE_OPERATOR IS NULL ");
                sentencia.append("AND STATE_NAME <> '' ");
                 sentencia.append("AND device_name <> 'Nombramientos' ");
                sentencia.append("ORDER BY ID");
              
                ps = getConexion().prepareStatement(sentencia.toString());
                rs = ps.executeQuery();
                
                while (rs.next()) {
                    Registro registro = new Registro();
                    registro.setClave(rs.getInt("ID"));
                    registro.setCredencial(rs.getString("CARD_NO"));
                    registro.setFecha(rs.getString("CREATE_TIME"));
                    registro.setAccion(rs.getString("STATE_NAME"));
                    registro.setRecinto(rs.getString("device_name"));
                    //Con estas lineas obtenemos el numero en formato wiegand de las credenciales, es decir como lo utilizan en API
                    if(rs.getString("CARD_NO").equals("") || rs.getString("CARD_NO").equals("--") ){
                       registro.setFolio(000);
                    }
                    else{
                       int numinterno = Integer.parseInt(rs.getString("CARD_NO"));
                       String res = Integer.toBinaryString(numinterno);
                       String weigand = res.substring(8);
                       int numcred = Integer.parseInt(weigand, 2);
                       registro.setFolio(numcred);
                       
                    }
                    registros.add(registro);
                    
                    
                }
                
            } catch (SQLException sqle) {
               log.warn("consultarRegistros, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
        
        return registros;
    }
    
    public void eliminarRegistro(int id) {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        
        if (conectar()) {
            try {
                sentencia.append("UPDATE ACC_MONITOR_LOG SET ");
                sentencia.append("DELETE_OPERATOR = 'Y', ");
                sentencia.append("DELETE_TIME = CURDATE() ");
                sentencia.append("WHERE ");
                sentencia.append("ID = ?");
                
                ps = getConexion().prepareStatement(sentencia.toString());
                
                ps.setInt(1, id);
                ps.executeUpdate();
                
            } catch (SQLException sqle) {
                log.warn("eliminarRegistro, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
    }
    
    public int verificarEntradaSalida(String numcred, String accion , String mov){
        
        int resultado = 0;
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (conectar()) {
           try{
              sentencia.append(" select count(*) ");
              sentencia.append(" from ZKECO_DB.ACC_MONITOR_LOG "); 
              sentencia.append(" where STATE_NAME <> ''  "); 
              sentencia.append("AND CREATE_TIME >= CURDATE() "); 
              sentencia.append(" AND device_name <> 'Nombramientos'  ");
               sentencia.append("AND DELETE_OPERATOR IS NULL ");
              sentencia.append(" and card_no =  ? "  );
              if(mov.equals("entrada")){
                sentencia.append(" and STATE_NAME = ? ");
              }
              else{
                  sentencia.append(" and STATE_NAME = ('CASETASSEMAVE-1 Salida') OR ('VIASSEMAVE-1 Salida') ");
              }
            
              ps = getConexion().prepareStatement(sentencia.toString());
              ps.setString(1, numcred);
              if(mov.equals("entrada")){
                ps.setString(2, accion);
              }  
             rs = ps.executeQuery();
             if (rs.next()){
              /* int  auxiliar = Integer.parseInt( rs.getString(1));
               if(auxiliar >=1){
                   resultado = 1;
               }*/
              resultado = 1;
             }
             
             
                
          }
          catch (SQLException sqle) {
            log.warn("Metodo verificarEntradaSalida, Error en sentancia SQL... " + sqle);
            resultado = -1;
          } 
          finally {
            desconectar();
          }
       }
        return resultado;
    }
    
}
