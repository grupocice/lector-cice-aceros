/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.dao.impl;

import com.cice.semave.dao.CheckDaoSemave;
import com.cice.semave.db.Conexion;
import com.cice.semave.entity.Check;
import com.cice.semave.entity.Empleado;
import com.cice.semave.proceso.Proceso;
import com.cice.semave.tools.Bitacora;
import com.cice.semave.tools.Correo;
import com.cice.semave.tools.Propiedades;
import com.cice.semave.tools.Respuesta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CheckDaoSemaveImpl extends Conexion implements CheckDaoSemave {
    
    private Logger log = LogManager.getLogger(CheckDaoSemaveImpl.class);
    RegistroDaoSemaveImpl registroMysql = new RegistroDaoSemaveImpl();
    EmpleadoRecintoDaoSemaveImpl empleadoAPI = new EmpleadoRecintoDaoSemaveImpl();
    Correo correo = new Correo();
    
    public CheckDaoSemaveImpl() {
        Propiedades propiedades = new Propiedades();
        propiedades.leerPropiedades("/com/cice/semave/gui/resources/semave.properties");
        this.setDriver(propiedades.getDriver());
        this.setUrl(propiedades.getUrl());
        this.setUsuario(propiedades.getUsuario());
        this.setPassword(propiedades.getPassword());
    }

    public ArrayList<Check> consultarChecadas() {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Check> checadas = new ArrayList<Check>();

        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("CKPCLAVE CLAVE, ");
                sentencia.append("CKPCREDENCIAL CREDENCIAL, ");
                sentencia.append("CKPFECHAREGISTRO FECHA, ");
                sentencia.append("CKPESTADO ESTADO, ");
                sentencia.append("CKPACCION ACCION ");
                sentencia.append("FROM CHECKPOINT ");
                sentencia.append("WHERE TO_CHAR(CKPFECHAREGISTRO, 'DD-MM-YYYY') = TO_CHAR(SYSDATE, 'DD-MM-YYYY') ");
                sentencia.append("AND CKPESTADO <> 'E' ");
                sentencia.append("ORDER BY FECHA");

                ps = getConexion().prepareStatement(sentencia.toString());
                rs = ps.executeQuery();

                while (rs.next()) {
                    Check check = new Check();
                    check.setClave(rs.getInt("CLAVE"));
                    check.setCredencial(rs.getString("CREDENCIAL"));
                    check.setFecha(rs.getDate("FECHA"));
                    check.setEstado(rs.getString("ESTADO"));
                    check.setAccion(rs.getString("ACCION"));

                    checadas.add(check);
                }

            } catch (SQLException sqle) {
                log.warn("Metodo consultarChecadas, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }

        return checadas;
    }

    public Respuesta existeCheckeo(String numcredencial/*, int turno*/, String accion, String fecha) {
        
       
        Respuesta objetoRespuesta = new Respuesta(); 
        String tipoMovimiento = "";
        int bandera = 0;
                if(accion.equals("CASETASSEMAVE-1 Entrada") || accion.equals("VIASSEMAVE-1 Entrada")){
                   tipoMovimiento = "Entrada";
                }
                else{
                    tipoMovimiento = "Salida";
                    bandera = 1;
                }

            //Con la variable bandera veirifcamos si esta checando entrada o salida
            switch(bandera){
                
                //Si esta checando Entrada
                case 0:
                   //Como esta cheando entrada verificamos que no tenga checada una entrada
                    int verificaEntrada = registroMysql.verificarEntradaSalida(numcredencial, accion, tipoMovimiento);
                    
                   switch(verificaEntrada){
                      //Si no tiene entrada lo dejamos que cheque entrada;
                      case 0:
                      objetoRespuesta.setResultado(0);
                      objetoRespuesta.setDescripcion("OK regsitro de entrada por " + accion);
                      log.warn(numcredencial + " ingresa por " + accion + " a las " + fecha);
                      break;
                      //Si tiene entrada buscamos que ya tenga su salida porque puede que esta persona ya entro, salio y ahora quiere volver a entrar
                      case 1:
                         int verificaSalida =  registroMysql.verificarEntradaSalida(numcredencial, accion, tipoMovimiento );
                         if(verificaSalida >= 1){
                             objetoRespuesta.setResultado(0);
                             objetoRespuesta.setDescripcion("OK registro de entrada por " + accion);
                             log.warn(numcredencial + " ingresa por " + accion + " a las " + fecha);
                         }
                         else if (verificaSalida == 0){
                            objetoRespuesta.setResultado(1);
                             objetoRespuesta.setDescripcion("ADVERTENCIA.. Este usuario esta checando entrada pero ya tiene una entrada registrada anteriormente ");
                             log.warn(numcredencial + " registra entrada por " + accion + " a las " + fecha + " pero ya tiene una entrada registrada anteriormente "  );
                             correo.enviarCorreoErrorPersonal(numcredencial, accion, fecha, objetoRespuesta.getDescripcion());
                         }
                      break;
                      //Si tiene problemas con la base de datos
                      default:
                           objetoRespuesta.setResultado(-1);
                             objetoRespuesta.setDescripcion("Problemas con la base de datos");
                      break;
                   }
                break;
                  
                //Si esta checando salida
                case 1:
                    //Como esta cheando salida necesitamos que tenga checada una entrada
                    int verEntrada = registroMysql.verificarEntradaSalida(numcredencial, accion, tipoMovimiento);
                    switch(verEntrada){
                        
                        case 0:
                        objetoRespuesta.setResultado(1);
                        objetoRespuesta.setDescripcion("ADVERTENCIA.. Este usuario esta checando salida,  pero no tiene un registro de Entrada "); 
                        log.warn(numcredencial + " registra salida por " + accion + " a las " + fecha  + " pero no cuenta con un registro de entrada");
                        correo.enviarCorreoErrorPersonal(numcredencial, accion, fecha, objetoRespuesta.getDescripcion());
                        break;
                        
                        case 1:
                        int verSalida = registroMysql.verificarEntradaSalida(numcredencial, accion, tipoMovimiento);
                        if(verSalida >= 0){
                           objetoRespuesta.setResultado(0);
                           objetoRespuesta.setDescripcion("OK registro de salida por " + accion); 
                           log.warn(numcredencial + " registro de salida por " + accion + " a las " + fecha);
                        }
                        else if(verSalida >= 1){
                           objetoRespuesta.setResultado(1);
                           objetoRespuesta.setDescripcion("ADVERTENCIA.. Este usuario tiene ya registro de salida "); 
                           log.warn(numcredencial + " registra salida por " + accion + " a las " + fecha  + " pero ya cuenta con un registro de salida");
                           correo.enviarCorreoErrorPersonal(numcredencial, accion, fecha, objetoRespuesta.getDescripcion());
                        }
                        else{
                            objetoRespuesta.setResultado(-1);
                           objetoRespuesta.setDescripcion("Problemas con la base de datos "); 
                        }
                    }
                    break;    
            }
        return objetoRespuesta;
       
    }
    
    
    public void registrarChecada(int clave, String credencial, String fecha, String estado, String accion,int folio, String dispositivo, String observaciones) {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        Empleado empleado = new Empleado();
        
        empleado = empleadoAPI.consultarEmpleadoRecinto(folio);
        if (conectar()) {
            try {
                String tipoMovimiento = "";
                if(accion.equals("CASETASSEMAVE-1 Entrada") || accion.equals("VIASSEMAVE-1 Entrada")){
                   tipoMovimiento = "Entrada";
                }
                else{
                    tipoMovimiento = "Salida";
                }
                
                sentencia.append("INSERT INTO CHECKPOINT( ");
                sentencia.append("CKPCLAVE, ");
                sentencia.append("CKPCREDENCIAL, ");
                sentencia.append("CKPFECHAREGISTRO, ");
                sentencia.append("CKPESTADO, ");
                sentencia.append("CKPACCION, ");
                sentencia.append("CKPFOLIO, ");
                sentencia.append("CKLECTOR, ");
                sentencia.append("CKPOBSERVACIONES,  ");
                sentencia.append("CKPNOMBRE,  ");
                sentencia.append("CKPAPELLIDOPAT,  ");
                sentencia.append("CKPAPELLIDOMAT) ");
            
                sentencia.append("VALUES(?,?,TO_DATE(?,'yyyy-mm-dd hh24:mi:ss'),?,?,?,?,?,?,?,?) ");

                ps = getConexion().prepareStatement(sentencia.toString());

                log.warn("-----------Credencial: "+ credencial+" -----------");
                log.warn("Credencial API ... "+clave);
                log.warn("Credencial CICE ... "+credencial);
                log.warn("Nombre " + empleado.getNombre());
                log.warn("Apellido Paterno " + empleado.getApellidopat());
                log.warn("Apellido Materno " + empleado.getApellidopmat());
                log.warn("Folio ... "+folio);
                log.warn("fecha ... "+fecha);
                log.warn("Accion ... "+tipoMovimiento);
                log.warn("Puerta " + accion);
                log.warn("Dispositivo " + dispositivo);
                
               
                ps.setInt(1, clave);
                ps.setString(2, credencial);
                ps.setString(3, fecha);
                ps.setString(4, estado);
                ps.setString(5, tipoMovimiento);
                ps.setInt(6, folio);
                ps.setString(7, dispositivo);
                ps.setString(8,observaciones);
                ps.setString(9,empleado.getNombre());
                ps.setString(10,empleado.getApellidopat());
                ps.setString(11,empleado.getApellidopmat());
                

                ps.execute();
                log.warn("Insertado correctamente");
               
            } catch (SQLException sqle) {
                log.warn("registrarChecada, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
    }

    public void eliminarChecada(int clave) {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;

        if (conectar()) {
            try {
                sentencia.append("UPDATE CHECKPOINT SET ");
                sentencia.append("CKPESTADO = 'E' ");
                sentencia.append("WHERE ");
                sentencia.append("CKPCLAVE = ? ");

                ps = getConexion().prepareStatement(sentencia.toString());

                ps.setInt(1, clave);

                ps.executeUpdate();

            } catch (SQLException sqle) {
                Bitacora.escribir("eliminarChecada, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
    }
    
    public int consultarTurno() {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        int turno = 0;

        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("PORTALWEB.FN_TURNO_NOMBRAMIENTO(SYSDATE) TURNO ");
                sentencia.append("FROM DUAL ");

                ps = getConexion().prepareStatement(sentencia.toString());
                rs = ps.executeQuery();
                if (rs.next()) {
                    turno = rs.getInt("TURNO");
                }

            } catch (SQLException sqle) {
                log.warn("consultarTurno, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
        return turno;
    }
    
    public int validaTurno() {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        int turno = 0;

        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("PORTALWEB.FN_VALIDA_TURNO(SYSDATE) TURNO ");
                sentencia.append("FROM DUAL ");

                ps = getConexion().prepareStatement(sentencia.toString());
                rs = ps.executeQuery();
                if (rs.next()) {
                    turno = rs.getInt("TURNO");
                }

            } catch (SQLException sqle) {
                Bitacora.escribir("validaTurno, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
        return turno;
    }
    
    public void notificarMotor(int empnum, String fecha, int turno, String trabnum) {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;

        if (conectar()) {
            try {
                sentencia.append("INSERT INTO WS_CICE_NOMBRAMIENTOS.XXTRABTORNIQUETE@APEX( ");
                sentencia.append("EMPNUM, ");
                sentencia.append("FECHA, ");
                sentencia.append("TURNO, ");
                sentencia.append("TRABNUM) ");
                sentencia.append("VALUES(?,?,?,?) ");

                ps = getConexion().prepareStatement(sentencia.toString());

                ps.setInt(1, empnum);
                ps.setString(2, fecha);
                ps.setInt(3, turno);
                ps.setString(4, trabnum);

                ps.execute();

            } catch (SQLException sqle) {
                Bitacora.escribir("notificarMotor, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
    }
    
    public boolean existeNotificacion(String empnum, String fecha, int turno, String trabnum) {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;

        boolean resultado = false;

        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("NEXT ");
                sentencia.append("FROM WS_CICE_NOMBRAMIENTOS.XXTRABTORNIQUETE@APEX ");
                sentencia.append("WHERE EMPNUM = ? ");
                sentencia.append("AND FECHA = ? ");
                sentencia.append("AND TURNO = ? ");
                sentencia.append("AND TRABNUM = ? ");

                ps = getConexion().prepareStatement(sentencia.toString());
                ps.setString(1, empnum);
                ps.setString(2, fecha);
                ps.setInt(3, turno);
                ps.setString(4, trabnum);
                
                rs = ps.executeQuery();

                if (rs.next()) {
                   resultado = true;
                }

            } catch (SQLException sqle) {
                Bitacora.escribir("existeNotificacion, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }

        return resultado;
    }
}

