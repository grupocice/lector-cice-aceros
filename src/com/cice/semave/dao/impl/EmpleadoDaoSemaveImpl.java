/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.dao.impl;

import com.cice.semave.dao.EmpleadoDaoSemave;
import com.cice.semave.db.Conexion;
import com.cice.semave.entity.Empleado;
import com.cice.semave.tools.Bitacora;
import com.cice.semave.tools.Propiedades;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ProyectosTI4
 */
public class EmpleadoDaoSemaveImpl extends Conexion implements EmpleadoDaoSemave {

   
    private Logger log = LogManager.getLogger(EmpleadoDaoSemaveImpl.class);
    
    public EmpleadoDaoSemaveImpl() {
        Propiedades propiedades = new Propiedades();
        propiedades.leerPropiedades("/com/cice/semave/gui/resources/semave.properties");
        this.setDriver(propiedades.getDriver());
        this.setUrl(propiedades.getUrl());
        this.setUsuario(propiedades.getUsuario());
        this.setPassword(propiedades.getPassword());
    }

    public Empleado consultarEmpleado(int claveregistro) {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Empleado empleado = new Empleado();

        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("CP.CKPCLAVE, ");
                sentencia.append("NOMBRE, ");
                sentencia.append("APELLIDOPAT, ");
                sentencia.append("APELLIDOMAT, ");
                sentencia.append("NUMCRED, ");
                sentencia.append("FOLIO, ");
                sentencia.append("C.TRABNUM, ");
                sentencia.append("C.EMPNUM, ");
                sentencia.append("TO_CHAR(CKPFECHAREGISTRO, 'DD/MM/YYYY') FECHACHECK, ");
                sentencia.append("CP.CKPACCION, ");
                sentencia.append("CP.CKPTURNO ");
                sentencia.append("FROM CREDAPI C,EMPLEADOS E,CHECKPOINT CP ");
                sentencia.append("WHERE C.EMPNUM = E.EMPNUM ");
                sentencia.append("AND C.TRABNUM = E.TRABNUM ");
                sentencia.append("AND FOLIO = CP.CKPFOLIO ");
                sentencia.append("AND C.EMPNUM = 1 ");
                sentencia.append("AND TO_CHAR(CKPFECHAREGISTRO, 'DD-MM-YYYY') = TO_CHAR(SYSDATE, 'DD-MM-YYYY') ");
                sentencia.append("AND CP.CKPESTADO = 'A' ");
                sentencia.append("AND CP.CKPCLAVE = ? ");
                sentencia.append("ORDER BY CKPCLAVE DESC");

                ps = getConexion().prepareStatement(sentencia.toString());
                ps.setInt(1, claveregistro);
                rs = ps.executeQuery();

                if (rs.next()) {
                    empleado.setClave(rs.getInt("CKPCLAVE"));
                    empleado.setCredencial(rs.getString("NUMCRED"));
                    empleado.setFolio(rs.getString("FOLIO"));
                    empleado.setNumtrabajador(rs.getString("TRABNUM"));
                    empleado.setEmpresa(rs.getInt("EMPNUM"));
                    empleado.setNombre(rs.getString("NOMBRE"));
                    empleado.setApellidopat(rs.getString("APELLIDOPAT"));
                    empleado.setApellidopmat(rs.getString("APELLIDOMAT"));
                    empleado.setFechachecada(rs.getString("FECHACHECK"));
                    empleado.setTiporegistro(rs.getString("CKPACCION"));
                    empleado.setTurno(rs.getInt("CKPTURNO"));
                }

            } catch (SQLException sqle) {
                log.warn("consultarEmpleado, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
        return empleado;
    }
    
    public ArrayList<Empleado> consultarEmpleados() {
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Empleado> personal = new ArrayList<Empleado>();

        if (conectar()) {
            try {
                sentencia.append("SELECT ");
                sentencia.append("CP.CKPCLAVE, ");
                sentencia.append("NOMBRE, ");
                sentencia.append("APELLIDOPAT, ");
                sentencia.append("APELLIDOMAT, ");
                sentencia.append("NUMCRED, ");
                sentencia.append("FOLIO, ");
                sentencia.append("C.TRABNUM, ");
                sentencia.append("C.EMPNUM, ");
                sentencia.append("TO_CHAR(CKPFECHAREGISTRO, 'DD/MM/YYYY HH24:MI:SS') FECHACHECK, ");
                sentencia.append("CP.CKPACCION ");
               // sentencia.append("CP.CKPTURNO ");
                sentencia.append("FROM NOMBRA.CREDAPI C, NOMBRA.EMPLEADOS E, CHECKPOINT CP ");
                sentencia.append("WHERE C.EMPNUM = E.EMPNUM ");
                sentencia.append("AND C.TRABNUM = E.TRABNUM ");
                sentencia.append("AND FOLIO = CP.CKPFOLIO ");
                sentencia.append("AND C.EMPNUM = 1 ");
                sentencia.append("AND TO_CHAR(CKPFECHAREGISTRO, 'DD-MM-YYYY') = TO_CHAR(SYSDATE, 'DD-MM-YYYY') ");
                sentencia.append("AND CP.CKPESTADO = 'A' ");
                sentencia.append("ORDER BY CKPCLAVE DESC");

                ps = getConexion().prepareStatement(sentencia.toString());
                rs = ps.executeQuery();

                while (rs.next()) {

                    Empleado empleado = new Empleado();
                    empleado.setClave(rs.getInt("CKPCLAVE"));
                    empleado.setCredencial(rs.getString("NUMCRED"));
                    empleado.setFolio(rs.getString("FOLIO"));
                    empleado.setNumtrabajador(rs.getString("TRABNUM"));
                    empleado.setEmpresa(rs.getInt("EMPNUM"));
                    empleado.setNombre(rs.getString("NOMBRE"));
                    empleado.setApellidopat(rs.getString("APELLIDOPAT"));
                    empleado.setApellidopmat(rs.getString("APELLIDOMAT"));
                    empleado.setFechachecada(rs.getString("FECHACHECK"));
                    empleado.setTiporegistro(rs.getString("CKPACCION"));
                    empleado.setObservaciones("OK");
                    personal.add(empleado);
                    log.warn("-------------Ok metodo consultarEmpleados-------------------- ");
                }

            } catch (SQLException sqle) {
                log.warn("consultarEmpleados, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
                 
            }
        }
        return personal;
    }
}

