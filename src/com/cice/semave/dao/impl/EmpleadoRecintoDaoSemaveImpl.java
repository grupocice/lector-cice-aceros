/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.dao.impl;

import com.cice.semave.dao.EmpleadoRecintoDaoSemave;
import com.cice.semave.db.Conexion;
import com.cice.semave.entity.Empleado;
import com.cice.semave.tools.Bitacora;
import com.cice.semave.tools.Propiedades;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmpleadoRecintoDaoSemaveImpl extends Conexion implements EmpleadoRecintoDaoSemave {

    private Logger log = LogManager.getLogger(EmpleadoRecintoDaoSemaveImpl.class);
    
    public EmpleadoRecintoDaoSemaveImpl() {
        
        Propiedades propiedades = new Propiedades();
        propiedades.leerPropiedades("/com/cice/semave/gui/resources/sqlserverdatabase.properties");
        this.setDriver(propiedades.getDriver());
        this.setUrl(propiedades.getUrl());
        this.setUsuario(propiedades.getUsuario());
        this.setPassword(propiedades.getPassword());
    }

    public Empleado consultarEmpleadoRecinto(int numerocredencial) {
        Empleado empleado = new Empleado();
       // if(numerocredencial == 1){
            
        StringBuilder sentencia = new StringBuilder();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
       

        if (conectar()) {
            try {
                sentencia.append("SELECT DISTINCT ");
                sentencia.append("B.ID FOLIO, ");
                sentencia.append("E.FIRSTNAME NOMBRE, ");
                sentencia.append("E.MIDNAME APELLIDOMAT, ");
                sentencia.append("E.LASTNAME APELLIDOPAT ");
                sentencia.append("FROM ACCESSCONTROL.DBO.EMP E, ");
                sentencia.append("ACCESSCONTROL.DBO.BADGE B ");
                sentencia.append("WHERE E.ID = B.EMPID ");
                sentencia.append("AND B.ID = ? ");

                ps = getConexion().prepareStatement(sentencia.toString());
                ps.setInt(1, numerocredencial);
                rs = ps.executeQuery();

                if (rs.next()) {
                    empleado.setFolio(rs.getString("FOLIO"));
                    empleado.setNombre(rs.getString("NOMBRE"));
                    empleado.setApellidopat(rs.getString("APELLIDOPAT"));
                    empleado.setApellidopmat(rs.getString("APELLIDOMAT"));
                }

            } catch (SQLException sqle) {
                log.warn("consultarEmpleadoRecinto, Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
       // }
        //else{
        //empleado.setNombre("generico");
       // empleado.setApellidopat("lopez");
      //  empleado.setApellidopmat("aguirre");
        //}
        return empleado;
    }
}

