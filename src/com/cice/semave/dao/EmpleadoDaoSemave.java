
package com.cice.semave.dao;

import com.cice.semave.entity.Empleado;
import java.util.ArrayList;

public interface EmpleadoDaoSemave {

    public Empleado consultarEmpleado(int claveregistro);
    
    public ArrayList<Empleado> consultarEmpleados();
}

