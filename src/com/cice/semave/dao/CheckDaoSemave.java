
package com.cice.semave.dao;

import com.cice.semave.entity.Check;
import com.cice.semave.tools.Respuesta;
import java.util.ArrayList;


public interface CheckDaoSemave {
    
    public ArrayList<Check> consultarChecadas();
    
    public Respuesta existeCheckeo(String numcredencial, String accion, String fecha);

    public void registrarChecada(int clave, String credencial, String fecha, String estado, String accion, int folio, String recinto, String observaciones);

    public void eliminarChecada(int clave);
    
    public int consultarTurno();
    
    public int validaTurno();
    
    public void notificarMotor(int empnum, String fecha, int turno, String trabnum);
    
    public boolean existeNotificacion(String empnum, String fecha, int turno, String trabnum);
}
