/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.dao;

import com.cice.semave.entity.Empleado;

public interface EmpleadoRecintoDaoSemave{

    public Empleado consultarEmpleadoRecinto(int numerocredencial);
}

