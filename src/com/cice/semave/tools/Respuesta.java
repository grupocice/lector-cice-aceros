/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.tools;


public class Respuesta {
    
    
    private int resultado;
    private String descripcion;

   
    public String getDescripcion() {
        return descripcion;
    }

    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
    public int getResultado() {
        return resultado;
    }

    
    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
}
