/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class Archivo {

    private String nombre;
    private String ruta;
    private String tipo;
    private long tamanio;

    public Archivo() {
    }

    public Archivo(String nombre, String ruta) {
        this.nombre = nombre;
        this.ruta = ruta;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getRuta() {
        return ruta;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTamanio(long tamanio) {
        this.tamanio = tamanio;
    }

    public long getTamanio() {
        return tamanio;
    }

    public void escribir(String cadena) {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(ruta + nombre, true));
            out.println(cadena);
            out.close();
        } catch (IOException e) {
            System.out.println("IOException : escribir(cadena) " + e.getMessage());
        }
    }

    public void escribir(InputStream input) {
        FileOutputStream output = null;

        try {
            output = new FileOutputStream(ruta + nombre);
            byte[] array = new byte[10240000];
            int leido = input.read(array);

            while (leido > 0) {
                output.write(array, 0, leido);
                leido = input.read(array);
            }

            input.close();
            output.close();
        } catch (IOException e) {
            System.out.println("IOException : escribir(InputStream) : " + e.getMessage());
        }
    }

    public boolean mover(String newruta, String newnombre) {
        boolean exito;

        File origen = new File(ruta + nombre);
        File destino = new File(newruta + newnombre);

        if (!(new File(newruta).exists())) {
            new File(newruta).mkdirs();
        }

        exito = origen.renameTo(destino);

        if (exito) {
            ruta = newruta;
            nombre = newnombre;
        }

        return exito;
    }

    public void borrar() {
        File file = new File(ruta + nombre);

        file.delete();
    }

    public static void escribir(String ruta, String nombre, String cadena) {
        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(ruta + nombre, true));
            out.println(cadena);
            out.close();
        } catch (IOException e) {
            System.out.println("IOException : escribir(ruta,nombre,cadena) " + e.getMessage());
        }
    }

    public static void escribir(String ruta, String nombre, InputStream input) {
        FileOutputStream output = null;
        int datos = -1;

        try {
            output = new FileOutputStream(ruta + nombre);

            do {
                datos = input.read();

                if (datos != -1) {
                    output.write(datos);
                }
            } while (datos != -1);

            input.close();
            output.close();
        } catch (IOException e) {
            System.out.println("IOException : escribir(ruta,nombre,InputStream) : " + e.getMessage());
        }
    }
}
