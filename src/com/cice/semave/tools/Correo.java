/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.tools;

import com.cice.semave.dao.impl.CheckDaoSemaveImpl;
import com.cice.semave.db.Conexion;
import com.cice.semave.entity.Empleado;
import com.cice.semave.entity.Registro;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Correo extends Conexion {

     private Logger log = LogManager.getLogger(CheckDaoSemaveImpl.class);
     
    public Correo() {
        this.setDriver("oracle.jdbc.driver.OracleDriver");
        this.setUrl("jdbc:oracle:thin:@10.70.10.200:1521:admi");
        this.setUsuario("cice");
        this.setPassword("cice");
    }

    public void enviarCorreoErrorPersonal(Empleado emp) {
       /* String mensaje = "";

        if (conectar()) {
            try {
                CallableStatement cs = getConexion().prepareCall("{? = call CICE_EMAIL_NOTIFY_PKG.CICE_EMAIL_OUT_FN(?,?,?,?,?,?,?)}");

                mensaje = mensaje + " El siguiente personal no esta correctamente relacionado, favor de verificar. \n\n";
                mensaje = mensaje + " Datos del empleado: \n\n";
                mensaje = mensaje + " Credencial: " + emp.getFolio() + "\n";
                mensaje = mensaje + " Nombre: " + emp.getNombre() + "\n";
                mensaje = mensaje + " Apellido materno: " + emp.getApellidopmat() + "\n";
                mensaje = mensaje + " Apellido paterno: " + emp.getApellidopat() + "\n";
                mensaje = mensaje + " Para relacionar esta credencial haga clic en la siguiente direccion: http://www.grupocice.com/nombramientos/consultacredencial/" + "\n";
                mensaje = mensaje + "\n\n\n";

                cs.registerOutParameter(1, Types.INTEGER);
                cs.setString(2, "jfnombramientos@grupocice.com;nombramientos@grupocice.com;grelaciones@grupocice.com;jrdominguez@grupocice.com;hgamboa@grupocice.com");
                cs.setString(3, "Error de personal con credencial no relacionada");
                cs.setNull(4, Types.NULL);
                cs.setNull(5, Types.NULL);
                cs.setString(6, "Error de personal con credencial no relacionada: "+emp.getFolio());
                cs.setString(7, mensaje);
                cs.setString(8, "Registro de personal nombrado");

                cs.execute();

                cs.getInt(1);

            } catch (SQLException sqle) {
                Bitacora.escribir("Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }*/
    }
    
    public void enviarCorreoErrorPersonal(Registro reg) {
       /* String mensaje = "";

        if (conectar()) {
            try {
                CallableStatement cs = getConexion().prepareCall("{? = call CICE_EMAIL_NOTIFY_PKG.CICE_EMAIL_OUT_FN(?,?,?,?,?,?,?)}");

                mensaje = mensaje + " El siguiente personal no esta correctamente relacionado, favor de verificar. \n\n";
                mensaje = mensaje + " Datos del empleado: \n\n";
                mensaje = mensaje + " Credencial: " + reg.getFolio() + "\n";
                mensaje = mensaje + " No se encontro ningun registro de nombre en API para esta credencial" + "\n\n\n";
                mensaje = mensaje + " Para relacionar esta credencial haga clic en la siguiente direccion: http://www.grupocice.com/nombramientos/consultacredencial/" + "\n";
                mensaje = mensaje + "\n\n\n";

                cs.registerOutParameter(1, Types.INTEGER);
                cs.setString(2, "jfnombramientos@grupocice.com;nombramientos@grupocice.com;grelaciones@grupocice.com;jrdominguez@grupocice.com;hgamboa@grupocice.com");
                cs.setString(3, "Error de personal con credencial no relacionada");
                cs.setNull(4, Types.NULL);
                cs.setNull(5, Types.NULL);
                cs.setString(6, "Error de personal con credencial no relacionada: "+reg.getFolio());
                cs.setString(7, mensaje);
                cs.setString(8, "Registro de personal nombrado");

                cs.execute();

                cs.getInt(1);

            } catch (SQLException sqle) {
                Bitacora.escribir("Error en sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }*/
    }
    
    public void enviarCorreoErrorPersonal(String numeroCredencial, String accion, String fecha, String descripcion ) {
        String mensaje = "";

        if (conectar()) {
            try {
                CallableStatement cs = getConexion().prepareCall("{? = call CICE_EMAIL_NOTIFY_PKG.CICE_EMAIL_OUT_FN(?,?,?,?,?,?,?)}");

                mensaje = mensaje + " El siguiente personal presenta una anomalia al checar registro, favor de verificar. \n\n";
                mensaje = mensaje + " CREDENCIAL: " + numeroCredencial + "\n";
                mensaje = mensaje + " FECHA DEL EVENTO: " + fecha + "\n";
                mensaje = mensaje + " PUERTA DE REGISTRO: " + accion + "\n";
                mensaje = mensaje +   descripcion + "\n\n\n" ;
               
                mensaje = mensaje + "\n\n\n";

                cs.registerOutParameter(1, Types.INTEGER);
                cs.setString(2, "jrproyectosti@grupocice.com;jrRRdominguez@grupocice.com;");
                cs.setString(3, "ALERTA EN REGISTRO DE ENTRADA/SALIDA A CICE ACEROS");
                cs.setNull(4, Types.NULL);
                cs.setNull(5, Types.NULL);
                cs.setString(6, "Credencial: " + numeroCredencial);// titulo
                cs.setString(7, mensaje); //mensaje
                cs.setString(8, "Registro de control de acceso CICE ACEROS");

                cs.execute();

                cs.getInt(1);

            } catch (SQLException sqle) {
                log.warn("Error en el metodo enviarCorreoErrorPersonal,  sentancia SQL... " + sqle);
            } finally {
                desconectar();
            }
        }
    }
}

