
package com.cice.semave.tools;


import java.io.IOException;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Propiedades {

    private String driver;
    private String url;
    private String usuario;
    private String password;
    private Logger log = LogManager.getLogger(Propiedades.class);
    
    public Propiedades() {
    }

    public void leerPropiedades(String archivo) {
        Properties propiedades = new Properties();
        try {
            propiedades.load(getClass().getResourceAsStream(archivo));

            setDriver(propiedades.getProperty("jdbc.drivers"));
            setUrl(propiedades.getProperty("jdbc.url"));
            setUsuario(propiedades.getProperty("jdbc.usuario"));
            setPassword(propiedades.getProperty("jdbc.password"));
        } catch (IOException ioex) {
           log.warn("leerPropiedades, Error de Entrada/Salida... " + ioex);
        }
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

