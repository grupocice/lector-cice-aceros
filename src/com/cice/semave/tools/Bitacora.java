/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.tools;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Bitacora {

    private static String ruta;

    public Bitacora() {
    }

    public static void escribir(String cadena) {
        Archivo.escribir(ruta, generarNombre(), new Date().toString() + " >> " + cadena);
    }

    private static String generarNombre() {
        GregorianCalendar calendario = new GregorianCalendar();
        String dia = null;
        String mes = null;
        String anio = null;

        dia = String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
        mes = String.valueOf(calendario.get(Calendar.MONTH) + 1);
        anio = String.valueOf(calendario.get(Calendar.YEAR));

        if (mes.length() == 1) {
            mes = "0" + mes;
        }

        return anio + "_" + mes + "_" + dia + ".txt";
    }

    public static void setRuta(String rutaparam) {
        ruta = rutaparam;
    }
}

