/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Fecha {

    public void Fecha() {
    }

    public Date toDate(String fecha) {
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date nuevaFecha = null;
        try {
            nuevaFecha = formatoFecha.parse(fecha);
        } catch (ParseException pe) {
            Bitacora.escribir("Error al formatear fecha... " + pe);
        }
        return nuevaFecha;
    }

    public String covertirFormatoFecha(String formato1, String formato2, String fecha) {

        SimpleDateFormat formatoFecha1 = new SimpleDateFormat(formato1);
        SimpleDateFormat formatoFecha2 = new SimpleDateFormat(formato2);

        String nuevaFecha = null;

        try {
            nuevaFecha = formatoFecha2.format(formatoFecha1.parse(fecha));
        } catch (ParseException pe) {
            Bitacora.escribir("Error al formatear fecha... " + pe);
        }
        return nuevaFecha;
    }

    public String fechaActual() {
        Date now = new Date();
        DateFormat df = DateFormat.getDateInstance();

        return df.format(now);
    }
}

