/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.proceso;

import com.cice.semave.dao.CheckDaoSemave;
import com.cice.semave.dao.EmpleadoRecintoDaoSemave;
import com.cice.semave.dao.RegistroDaoSemave;
import com.cice.semave.dao.impl.CheckDaoSemaveImpl;
import com.cice.semave.dao.impl.EmpleadoDaoSemaveImpl;
import com.cice.semave.dao.impl.EmpleadoRecintoDaoSemaveImpl;
import com.cice.semave.dao.impl.RegistroDaoSemaveImpl;
import com.cice.semave.entity.Empleado;
import com.cice.semave.entity.Registro;
import com.cice.semave.lector.Manejador;
import com.cice.semave.tools.Bitacora;
import com.cice.semave.tools.Correo;
import com.cice.semave.tools.Respuesta;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Proceso {
    
    private Logger log = LogManager.getLogger(Proceso.class);
    
    public Proceso() {
    }
   
    public void relacionarRegistro(Registro registro) {
        CheckDaoSemave checkdao = new CheckDaoSemaveImpl();
        Respuesta respuesta = new Respuesta();
        EmpleadoDaoSemaveImpl empleadodao = new EmpleadoDaoSemaveImpl();
        
        // Obtenemos la respuesta d ela base de datos
        respuesta = checkdao.existeCheckeo(registro.getCredencial(), registro.getAccion(), registro.getFecha());
        
        checkdao.registrarChecada(registro.getClave(), registro.getCredencial(), registro.getFecha(), "A", registro.getAccion(), registro.getFolio(), registro.getRecinto(), respuesta.getDescripcion());
            /*Empleado empleado = empleadodao.consultarEmpleado(registro.getClave());
            if (empleado.getClave() == 0) {
                //Bitacora.escribir("El empleado con el siguiente numero de credencial no esta relacionado... " + registro.getFolio());
                log.warn("El empleado con el siguiente numero de credencial no esta relacionado... " + registro.getFolio());
                checkdao.eliminarChecada(registro.getClave());
                
                EmpleadoRecintoDaoSemave empleadorecintodao = new EmpleadoRecintoDaoSemaveImpl();
                Empleado empleadorecinto = empleadorecintodao.consultarEmpleadoRecinto(registro.getFolio());
                Correo correo = new Correo();

                if(empleadorecinto.getFolio()!= null)
                   correo.enviarCorreoErrorPersonal(empleadorecinto);
                else
                   correo.enviarCorreoErrorPersonal(registro);
            } else {
                if (checkdao.validaTurno() != -1){
                    Bitacora.escribir("El empleado a insertar en el motor es... " + registro.getClave() + " " + empleado.getEmpresa() + " " + empleado.getFechachecada() + " " + empleado.getTurno() + " " + empleado.getNumtrabajador());
                    checkdao.notificarMotor(empleado.getEmpresa(), empleado.getFechachecada(), empleado.getTurno(), empleado.getNumtrabajador());
                }
            }*/
        

        RegistroDaoSemave regdao = new RegistroDaoSemaveImpl();
        regdao.eliminarRegistro(registro.getClave());
    }
}
