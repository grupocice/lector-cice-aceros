
package com.cice.semave.entity;


public class Registro {
    
    private int clave;
    private String credencial;
    private int folio;
    private String fecha;
    private String estado;
    private String accion;
    private String recinto;

    public void Registro() {
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getCredencial() {
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }
    public void setRecinto(String recinto){
        this.recinto  = recinto;
    }
    
    public String getRecinto(){
        return recinto;
    }
}
