
package com.cice.semave.entity;


public class Empleado {
    
    private int clave;
    private int empresa;
    private String nombre;
    private String apellidopat;
    private String apellidopmat;
    private String folio;
    private String credencial;
    private String numtrabajador;
    private String tiporegistro;
    private String fechachecada;
    private int turno;
    private String observaciones;

    public void Empleado() {
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public int getEmpresa() {
        return empresa;
    }

    public void setEmpresa(int empresa) {
        this.empresa = empresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidopat() {
        return apellidopat;
    }

    public void setApellidopat(String apellidopat) {
        this.apellidopat = apellidopat;
    }

    public String getApellidopmat() {
        return apellidopmat;
    }

    public void setApellidopmat(String apellidopmat) {
        this.apellidopmat = apellidopmat;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCredencial() {
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }

    public String getNumtrabajador() {
        return numtrabajador;
    }

    public void setNumtrabajador(String numtrabajador) {
        this.numtrabajador = numtrabajador;
    }
    
    public String getTiporegistro() {
        return tiporegistro;
    }

    public void setTiporegistro(String tiporegistro) {
        this.tiporegistro = tiporegistro;
    }

    public String getFechachecada() {
        return fechachecada;
    }

    public void setFechachecada(String fechachecada) {
        this.fechachecada = fechachecada;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
