
package com.cice.semave.db;

import com.cice.semave.dao.impl.CheckDaoSemaveImpl;
import com.cice.semave.tools.Bitacora;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Conexion {
   
    private Connection conexion;
    private String driver;
    private String url;
    private String usuario;
    private String password;
    
    private Logger log = LogManager.getLogger(Conexion.class);

    public Conexion() {
        conexion = null;
        driver = null;
        url = null;
        usuario = null;
        password = null;
    }

    public boolean conectar() {
        boolean ban = false;
        try {
            setConexion(DriverManager.getConnection(url, usuario, password));
            ban = true;
        } catch (SQLException sqle) {
            log.warn("Error al intentar conectar a la BD... " + sqle);
        }

        return ban;
    }

    public void desconectar() {
        try {
            if (getConexion() != null) {
                getConexion().close();
            }
        } catch (SQLException sqle) {
            Bitacora.escribir("Error al intentar desconectar de la BD... " + sqle);
        }
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
