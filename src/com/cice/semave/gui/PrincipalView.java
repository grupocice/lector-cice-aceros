
package com.cice.semave.gui;

import com.cice.semave.dao.EmpleadoDaoSemave;
import com.cice.semave.dao.impl.EmpleadoDaoSemaveImpl;
import com.cice.semave.entity.Empleado;
import com.cice.semave.lector.Manejador;
import com.cice.semave.tools.Bitacora;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.table.DefaultTableModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ProyectosTI4
 */
public class PrincipalView extends javax.swing.JFrame {
    
    private Logger log = LogManager.getLogger(PrincipalView.class);
    
    Manejador manejador;
    
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JMenuBar menuBar;
    
    
    
    static Image image = Toolkit.getDefaultToolkit().getImage("src"+ File.separator + "com" + File.separator + "cice" + File.separator + "nombramientos" + File.separator + "gui" + File.separator + "resources" + File.separator + "icontray.gif" + File.separator);
    static TrayIcon trayIcon = new TrayIcon(image, "CICE Aceros 1.0");

    
    public PrincipalView() {
        
        initComponents();
       // Bitacora.setRuta("D:" + File.separator + "Bitacoras" + File.separator + "LectorSemave" + File.separator );
        manejador = new Manejador(this);
        actualizarPool();
        
        if (SystemTray.isSupported()) {
            
            SystemTray tray = SystemTray.getSystemTray();
            trayIcon.setImageAutoSize(true);
            trayIcon.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    trayIcon.displayMessage("CICE ACEROS", "En linea", TrayIcon.MessageType.INFO);
                }
            });

            try {
                tray.add(trayIcon);
            } catch (AWTException awte) {
                //Bitacora.escribir("Error AWT... " + awte);
                log.warn("Error AWT... " + awte);
            }
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                inicializarMotor();
            }
        });
    }
    
    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrincipalView().setVisible(true);
            }
        });
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Lector Credenciales - CICE ACEROS");
        setResizable(false);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Clave", "Numero de credencial", "Nombre", "Apellido paterno", "Apellido materno", "Movimiento", "Fecha/Hora", "observaciones"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1287, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Registro de checadas", jPanel1);

        jButton1.setText("Iniciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Detener");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Estado del servicio:");

        jLabel2.setText("status");

        fileMenu.setMnemonic('a');
        fileMenu.setText("Archivo");

        exitMenuItem.setMnemonic('s');
        exitMenuItem.setText("Salir");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 949, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(50, 50, 50))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(615, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(25, 25, 25))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jTabbedPane1)
                    .addGap(0, 80, Short.MAX_VALUE)))
        );

        setSize(new java.awt.Dimension(1308, 722));
        setLocationRelativeTo(null);
    }// </editor-fold>                        

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {                                             
        System.exit(0);
    }                                            

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        try {
            manejador.reanudarMonitoreo();
            jButton1.setEnabled(false);
            jButton2.setEnabled(true);
            estadoIniciado();
        } catch (Exception e) {
           // Bitacora.escribir("Error al iniciar el monitor... " + e);
           log.warn("Error al iniciar el monitor... " + e);
        }
    }                                        

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        
        try {
            manejador.detenerMonitoreo();
            jButton1.setEnabled(true);
            jButton2.setEnabled(false);
            estadoDetenido();
        } catch (Exception e) {
           // Bitacora.escribir("Error al detener el monitor... " + e);
           log.warn("Error al detener el monitor... " + e);
        }
    }                                        
    
    public void inicializarMotor() {
        jButton1.setEnabled(true);
        jButton2.setEnabled(false);
        
        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setText("Detenido");

        try {
            manejador.iniciarMonitoreo();
            manejador.detenerMonitoreo();
        } catch (Exception e) {
           // Bitacora.escribir("Error al inicializar el monitor... " + e);
           log.warn("Error al inicializar el monitor... " + e);
        }
    }
    
    public void actualizarPool() {
        EmpleadoDaoSemave empdao = new EmpleadoDaoSemaveImpl();
        ArrayList<Empleado> listaper = empdao.consultarEmpleados();

        Iterator it = listaper.iterator();
        DefaultTableModel modelo = (DefaultTableModel) jTable1.getModel();

        while (it.hasNext()) {
            Empleado e = (Empleado) it.next();
            Object[] fila = new Object[8];
            fila[0] = e.getClave();
            fila[1] = e.getFolio();
            fila[2] = e.getNombre();
            fila[3] = e.getApellidopat();
            fila[4] = e.getApellidopmat();
            fila[5] = e.getTiporegistro();
            fila[6] = e.getFechachecada();
            fila[7] = e.getObservaciones();
            modelo.addRow(fila);
        }
    }
    
    public void limpiarPool() {
        
        DefaultTableModel modelo = (DefaultTableModel) jTable1.getModel();
        for (int row = modelo.getRowCount() - 1; row >= 0; row--) {
            modelo.removeRow(row);
        }
    }
    
    public void estadoIniciado() {
        jLabel2.setForeground(new java.awt.Color(0, 255, 0));
        jLabel2.setText("Iniciado");
    }

    public void estadoDetenido() {
        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setText("Detenido");
    }

    public void estadoInaccesible() {
        jLabel2.setForeground(new java.awt.Color(255, 102, 0));
        jLabel2.setText("Lector de credenciales inaccesible");
    }
                  
    
                 

}

