/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cice.semave.lector;

import com.cice.semave.dao.RegistroDaoSemave;
import com.cice.semave.dao.impl.RegistroDaoSemaveImpl;
import com.cice.semave.entity.Registro;
import com.cice.semave.gui.PrincipalView;
import com.cice.semave.proceso.Proceso;
import com.cice.semave.tools.Bitacora;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Manejador extends Thread {

    PrincipalView cliente;
    private boolean continuar = true;
    private boolean suspender = false;
    private boolean inaccesible = false;
    private Logger log = LogManager.getLogger(Manejador.class);
    
     
    public Manejador(PrincipalView cliente) {
        this.cliente = cliente;
    }

    public void iniciarMonitoreo() {
        this.start();
    }

    public void detenerMonitoreo() {
        suspender = true;
    }

    public void reanudarMonitoreo() {
        log.warn("Lector iniciado...");
        synchronized (this) {
            suspender = false;
            this.notify();
        }
    }

    public void run() {
       // Bitacora.escribir("Inicializando lector de credenciales...");
       log.warn("Inicializando lector de credenciales...");
        while (continuar) {

            try {
                synchronized (this) {
                    if (suspender) {
                        //Bitacora.escribir("Lector detenido...");
                        log.warn("Lector detenido...");
                        this.wait();
                    }
                }

                if (inaccesible) {
                    //Bitacora.escribir("El servicio es inaccesible el lector se suspendera momentaneamente...");
                    log.warn("El servicio es inaccesible el lector se suspendera momentaneamente...");
                    this.sleep(500000);
                } else {
                    this.sleep(3);
                }

            } catch (InterruptedException ie) {
               // Bitacora.escribir("Error al intentar detener el lector..." + ie);
                log.warn("Error al intentar detener el lector..." + ie);
            }
            if (continuar) {

                Proceso proceso = new Proceso();

                RegistroDaoSemave regdao = new RegistroDaoSemaveImpl();
                
                //Metodo que se trae los registros de las personas que checaron 
                ArrayList<Registro> listareg = regdao.consultarRegistros();

                Iterator it = listareg.iterator();
                if (!listareg.isEmpty()) {
                    int cont = 0;
                    while (it.hasNext() && !suspender) {
                        Registro reg = (Registro) it.next();

                        try {
                           // Bitacora.escribir("Insertamos los datos del empleado que checo...");
                           log.warn("Insertamos los datos del empleado que checo de sql a admi");
                            proceso.relacionarRegistro(reg);
                            cont++;
                            inaccesible = false;
                            cliente.estadoIniciado();
                        } catch (Exception e) {
                           // Bitacora.escribir("Error al intentar comunicacion con las BD del lector... " + e);
                            log.warn("Error al intentar comunicacion con las BD del lector... " + e);
                            inaccesible = true;
                            cliente.estadoInaccesible();
                            break;
                        }
                        //Bitacora.escribir("Actualizamos la tabla...");
                        log.warn("Actualizamos la tabla...");
                        cliente.limpiarPool();
                        log.warn("-----------------------------------");
                       // cliente.actualizarPool();
                    }
                }
            }
        }
    }
}

